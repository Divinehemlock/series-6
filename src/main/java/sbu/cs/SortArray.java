package sbu.cs;

public class SortArray {

    /**
     * sort an array with selection sort algorithm
     *
     * @param array  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] array, int size) {
        return null;
    }

    /**
     * sort an array with insertion sort algorithm
     *
     * @param array array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] array, int size) {
        return null;
    }

    /**
     * sort an array with merge sort algorithm
     *
     * @param array  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] array, int size) {
        return null;
    }

    /**
     * return position of given value in array which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param array   sorted array
     * @param value value to be found
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] array, int value) {
        return -1;
    }

    /**
     * return position of given value in array which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param array   sorted array
     * @param value value to be found
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] array, int value) {
        return -1;
    }
}
